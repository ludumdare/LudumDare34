﻿using UnityEngine;
using System.Collections;

public class TestPlayer : MonoBehaviour
{
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            GameManager.Instance.PlayerController.AddBodyPart();
        }

        if (Input.GetKeyUp(KeyCode.N))
        {
            GameManager.Instance.GameController.NextLevel();
        }
	}
}
