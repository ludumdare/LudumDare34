﻿using UnityEngine;
using System.Collections;

public enum MobTypes
{
    None,
    Human,
    Chicken,
    Dogs,
    Cats,
    Elfs,
    Santa
}
public class MobData : ScriptableObject
{
    public string MobName;
    public int MobPoints;
    public int MobWeight;
    
}
