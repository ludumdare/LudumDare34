﻿using UnityEngine;
using System.Collections;

public class StatsGuiController : GuiController
{
    public override void AfterOnEnable()
    {
        base.AfterOnEnable();
        GameManager.Instance.StatsGui = this;
    }
}
