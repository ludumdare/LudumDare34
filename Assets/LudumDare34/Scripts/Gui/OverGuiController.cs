﻿using UnityEngine;
using System.Collections;

public class OverGuiController : GuiController
{
    public void ReplayLevel()
    {
        GameManager.Instance.SoundController.PlaySound("beep");
        GameManager.Instance.GameController.ReplayLevel();
    }

    public override void AfterOnEnable()
    {
        base.AfterOnEnable();
        GameManager.Instance.OverGui = this;
    }
}
