﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameGuiController : GuiController
{
    public Text GameScore;
    public Text WormSegments;
    public Text EagleHealth;

    public void Refresh()
    {
        if (canvas.enabled)
        {
            GameScore.text = string.Format("{0}", GameManager.Instance.GameController.Score);
            WormSegments.text = string.Format("{0}", GameManager.Instance.PlayerController.Segments);
            EagleHealth.text = string.Format("{0}", GameManager.Instance.EagleController.Health);
        }
    }

    
    public override void AfterOnEnable()
    {
        base.AfterOnEnable();
        D.Trace("[GameGuiController] AfterOnEnable");
        GameManager.Instance.GameGui = this;
    }

}
