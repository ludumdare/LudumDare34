﻿using UnityEngine;
using System.Collections;

public class ThanksGuiController : GuiController
{

    public override void AfterOnEnable()
    {
        base.AfterOnEnable();
        GameManager.Instance.ThanksGui = this;
    }
}
