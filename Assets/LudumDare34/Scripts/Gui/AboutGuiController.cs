﻿using UnityEngine;
using System.Collections;

public class AboutGuiController : GuiController
{

    public override void AfterOnEnable()
    {
        base.AfterOnEnable();
        GameManager.Instance.AboutGui = this;
    }
}
