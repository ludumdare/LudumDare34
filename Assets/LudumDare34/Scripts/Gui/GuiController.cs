﻿using UnityEngine;
using System.Collections;

public class GuiController : MonoBehaviour
{
    protected Canvas canvas;

    public void Hide()
    {
        canvas.enabled = false;
    }

    public void Show()
    {
        canvas.enabled = true;
    }

    public virtual void AfterOnEnable()
    {
        D.Trace("[GuiController] AfterOnEnable");
    }

    void OnEnable()
    {
        D.Trace("[GuiController] OnEnable");
        canvas = transform.FindChild("Canvas").GetComponent<Canvas>();
        AfterOnEnable();
    }
}
