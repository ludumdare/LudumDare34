﻿using UnityEngine;
using System.Collections;

public class HomeGuiController : GuiController
{
    public void HomeButton()
    {
        GameManager.Instance.SoundController.PlaySound("beep");
        GameManager.Instance.GameController.StartHome();
    }

    public void PlayButton()
    {
        GameManager.Instance.SoundController.PlaySound("beep");
        GameManager.Instance.GameController.StartGame();
    }

    public void StatsButton()
    {
        GameManager.Instance.SoundController.PlaySound("beep");
        GameManager.Instance.GameController.StartStats();
    }

    public void OptionsButton()
    {
        GameManager.Instance.SoundController.PlaySound("beep");
        GameManager.Instance.GameController.StartOptions();
    }

    public void AboutButton()
    {
        GameManager.Instance.SoundController.PlaySound("beep");
        GameManager.Instance.GameController.StartAbout();
    }

    public override void AfterOnEnable()
    {
        base.AfterOnEnable();
        GameManager.Instance.HomeGui = this;
    }
}
