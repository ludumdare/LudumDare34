﻿using UnityEngine;
using System.Collections;

public class OptionsGuiController : GuiController
{
    public override void AfterOnEnable()
    {
        base.AfterOnEnable();
        GameManager.Instance.OptionsGui = this;
    }
}
