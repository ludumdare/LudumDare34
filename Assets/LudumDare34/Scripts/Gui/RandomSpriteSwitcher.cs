﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RandomSpriteSwitcher : MonoBehaviour
{
    public Sprite[] sprites;
    public int flickerAmount;
    public float flickerMax;

    private IEnumerator run()
    {
        while(true)
        {
            for (int i = 0; i < flickerAmount; i++)
            {
                GetComponent<Image>().sprite = sprites[Random.Range(0, 2)];
                yield return new WaitForSeconds(Random.Range(0.01f, flickerMax));
            }
            GetComponent<Image>().sprite = sprites[1];
            yield return new WaitForSeconds(3.0f);
        }
    }

    void Start()
    {
        StartCoroutine(run());
    }

}
