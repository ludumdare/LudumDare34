﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class WinGuiController : GuiController
{
    public Text WinGems;
    public Text WinIce;
    public Text WinJewels;
    public Text WinBody;
    public Text WinEarthlings;
    public Text WinScore;
    public Text GemBonus;
    public Text JewelBonus;
    public Text IceBonus;
    public Text BodyBonus;
    public Text EarthlingBonus;
    public Text Score;

    private Coroutine tally;

    private int currentScore;
    private int newScore;

    public void TallyScore()
    {
        currentScore = GameManager.Instance.GameController.Score;
        newScore = 0;
        calculateBonus();
        Score.text = GameManager.Instance.GameController.Score.ToString();
        tally = StartCoroutine(run());

    }

    private void calculateBonus()
    {
        PlayerController pc = GameManager.Instance.PlayerController;
        GameController gc = GameManager.Instance.GameController;

        int _segmentValue = 100;

        if (pc.Segments > 9)
        {
            _segmentValue = 250;
        }

        if (_segmentValue > 19)
        {
            _segmentValue = 500;
        }

        int _gemBonus = pc.CollectedGems * 15;
        int _jewelBonus = pc.CollectedJewels * 25;
        int _iceBonus = pc.CollectedIce * 50;
        int _earthlingBonus = pc.CollectedEarthlings * 10;
        int _bodyBonus = pc.Segments * _segmentValue;

        WinGems.text = pc.CollectedGems + "/" + gc.TotalGems;
        WinIce.text = pc.CollectedIce + "/" + gc.TotalIce;
        WinJewels.text = pc.CollectedJewels + "/" + gc.TotalJewels;
        WinBody.text = pc.Segments.ToString();
        WinEarthlings.text = pc.CollectedEarthlings + "/" + gc.TotalEarthlings;

        GemBonus.text = "+" + (_gemBonus);
        JewelBonus.text = "+" + (_jewelBonus);
        IceBonus.text = "+" + (_iceBonus);
        EarthlingBonus.text = "+" + (_earthlingBonus);
        BodyBonus.text = "+" + _bodyBonus;

        if (pc.CollectedGems > gc.TotalGems)
        {
            GemBonus.text = "+1500";
            _gemBonus = 1500;
        }

        if (pc.CollectedIce >= gc.TotalIce)
        {
            IceBonus.text = "+250";
            _iceBonus = 250;
        }

        if (pc.CollectedJewels >= gc.TotalJewels)
        {
            JewelBonus.text = "+500";
            _jewelBonus = 500;
        }

        if (pc.CollectedEarthlings >= gc.TotalEarthlings)
        {
            EarthlingBonus.text = "+5000";
            _earthlingBonus = 5000;
        }

        int _bonus = _gemBonus + _iceBonus + _jewelBonus + _earthlingBonus + _bodyBonus;

        gc.Score += _bonus;
        newScore = gc.Score;

    }

    public int TweenScore { get; set; }

    public IEnumerator run()
    {
        float _start = currentScore;
        float _end = newScore;

        yield return DOVirtual.Float(_start, _end, 3.0f, updateScore);
        yield return null;
    }

    public void updateScore(float score)
    {
        Score.text = Mathf.CeilToInt(score).ToString();
        GameManager.Instance.SoundController.PlaySound("scorebonus");
    }

    public void NextLevel()
    {
        StopCoroutine(tally);
        GameManager.Instance.SoundController.PlaySound("beep");
        GameManager.Instance.GameController.NextLevel();
    }

    public void ReplayLevel()
    {
        StopCoroutine(tally);
        GameManager.Instance.SoundController.PlaySound("beep");
        GameManager.Instance.GameController.ReplayLevel();
    }


    public override void AfterOnEnable()
    {
        base.AfterOnEnable();
        GameManager.Instance.WinGui = this;
    }
}
