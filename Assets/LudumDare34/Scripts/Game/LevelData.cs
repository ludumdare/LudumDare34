﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelData : ScriptableObject
{
    [SerializeField]
    public List<LevelItemData> data = new List<LevelItemData>();    
}
