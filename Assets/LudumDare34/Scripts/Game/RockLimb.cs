﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class RockLimb : MonoBehaviour
{
    public float AngleVariance;
    public float AngleDuration;
    public bool AngleRepeat;

    void Start()
    {
        transform.DOLocalRotate(new Vector3(0, 0, Random.Range(-AngleVariance, AngleVariance)), AngleDuration).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
    }
}
