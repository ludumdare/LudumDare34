﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LevelItem
{
    [SerializeField]
    public string LevelId;

    [SerializeField]
    public string LevelName;

    [SerializeField]
    public TextAsset LevelText;

    [SerializeField]
    public int LevelOrder;
}

