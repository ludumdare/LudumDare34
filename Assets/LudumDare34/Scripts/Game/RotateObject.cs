﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class RotateObject : MonoBehaviour
{
    public float RotateDuration;
    public bool RotateRandomStart;

	// Use this for initialization
	void Start () {
        float z = Random.Range(0, 360);
        transform.Rotate(new Vector3(0, 0, z));
        transform.DORotate(new Vector3(0, 0, 360), RotateDuration, RotateMode.LocalAxisAdd).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);	
	}
	
}
