﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MoveBone : MonoBehaviour
{

    public float BoneStart;
    public float BoneEnd;
    public float BoneDuration;

	// Use this for initialization
	void Start ()
    {
        Vector3 rs = new Vector3(0, 0, BoneStart);
        Vector3 re = new Vector3(0, 0, BoneEnd);
        transform.Rotate(rs);
        transform.DOLocalRotate(re, BoneDuration, RotateMode.Fast).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);	    
	}
	
}
