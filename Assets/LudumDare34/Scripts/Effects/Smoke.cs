﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Smoke : MonoBehaviour
{
    public Sprite smoke;
    public float life;

    void Start()
    {
        StartCoroutine(run());
    }

    private IEnumerator run()
    {
        float dir = Random.Range(0, 2);
        if (dir == 0)
        {
            dir = -360;
        }
        else
        {
            dir = 360;
        }

        transform.localScale = Vector3.one * 0.1f;
        transform.DORotate(new Vector3(0, 0, dir), life * 5, RotateMode.LocalAxisAdd).SetEase(Ease.Linear);
        transform.DOScale(Vector3.one * 3, life);
        Destroy(gameObject, life + 0.5f);
        yield return null;
    }
}
