﻿using UnityEngine;
using System.Collections;

public class SpriteScroller : MonoBehaviour 
{
    public float SpriteSpeed;
    public Vector3 SpriteDirection;
    public bool SpriteWrap;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.Translate(SpriteDirection * SpriteSpeed * Time.deltaTime);    
	}
}
