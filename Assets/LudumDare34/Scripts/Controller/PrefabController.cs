﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Prefab
{
    public string PrefabName;
    public GameObject PrefabObject;
}

public class PrefabController : MonoBehaviour
{
    public Prefab[] Prefabs;

    private Dictionary<string, GameObject> _prefabs;

    //  PUBLIC

    public GameObject GetPrefab(string name)
    {
        if (_prefabs.ContainsKey(name))
        {
            GameObject go = Instantiate(_prefabs[name]);
            return go;
        }
        else
        {
            Debug.Log(string.Format("- missing Prefab {0}", name));
            return null;
        }
    }

    public GameObject CreateObject(string name, Vector3 pos)
    {
        if (_prefabs.ContainsKey(name))
        {
            GameObject go = (GameObject)Instantiate(_prefabs[name], pos, Quaternion.identity);
            return go;
        }
        else
        {
            Debug.Log(string.Format("- missing Prefab {0}", name));
            return null;
        }
    }

    //  PRIVATE 

    private void loadPrefabs()
    {
        _prefabs = new Dictionary<string, GameObject>();

        foreach(Prefab prefab in Prefabs)
        {
            _prefabs.Add(prefab.PrefabName, prefab.PrefabObject);
        }
    }

    //  MONO

    void Start()
    {
    }

    void OnEnable()
    {
        GameManager.Instance.PrefabController = this;
        loadPrefabs();
    }
}
