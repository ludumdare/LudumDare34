﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SpriteTile;

public class PlayerController : PauseObject
{
    public float playerSpeed;
    public float rotateSpeed;
    public GameObject bodyPiece;
    public float bodySpacing;

    public int CollectedGems { get; set; }
    public int CollectedJewels { get; set; }
    public int CollectedIce { get; set; }
    public int CollectedEarthlings { get; set; }
    public int CollectedRocks { get; set; }

    private float _currentSpeed;
    private int _playerWeight;
    private Coroutine c1;
    private Coroutine c2;

    private List<GameObject> bodySegments = new List<GameObject>();
    private List<Vector3> breadcrumbs = new List<Vector3>();
    private List<Vector3> angles = new List<Vector3>();

    //  PUBLIC

    public override void Activate()
    {
        base.Activate();

        CollectedJewels = 0;
        CollectedRocks = 0;
        CollectedGems = 0;
        CollectedEarthlings = 0;
        CollectedIce = 0;

        breadcrumbs.Clear();
        angles.Clear();
        bodySegments.Clear();

        breadcrumbs.Add(transform.position);   //  add head
        angles.Add(transform.eulerAngles);

        AddBodyPart();
        AddBodyPart();
        AddBodyPart();
        AddBodyPart();
        AddBodyPart();

        c1 = StartCoroutine(run());
        c2 = StartCoroutine(body());
    }

    public override void Deactivate()
    {
        base.Deactivate();

        if (c1 != null)
            StopCoroutine(c1);

        if (c2 != null)
            StopCoroutine(c2);

        foreach (GameObject go in bodySegments)
        {
            Destroy(go.gameObject);
        }
    }

    public void AddBodyPart()
    {
        D.Trace("[PlayerController] AddBodyPart");
        GameObject go = Instantiate(bodyPiece);
        bodySegments.Add(go);
        breadcrumbs.Add(bodySegments[bodySegments.Count - 1].transform.position);
        angles.Add(bodySegments[bodySegments.Count - 1].transform.eulerAngles);
        GameManager.Instance.GameGui.Refresh();
    }

    public void RemoveBodyPart()
    {
        if (bodySegments.Count > 0)
        {
            angles.RemoveAt(angles.Count-1);
            breadcrumbs.RemoveAt(breadcrumbs.Count-1);
            GameManager.Instance.PrefabController.CreateObject("smoke", bodySegments[bodySegments.Count-1].transform.position);
            Destroy(bodySegments[bodySegments.Count-1],5.0f);
            bodySegments.RemoveAt(bodySegments.Count-1);
        }
        else
        {
            GameManager.Instance.SoundController.PlaySound("death");
            GameManager.Instance.GameController.StartOver();
        }
    }

    public void StartingPosition(Int2 pos)
    {
        transform.position = pos.ToVector2();
    }

    public int Segments
    {
        get { return bodySegments.Count; }
    }

    //  PRIVATE

    private IEnumerator run()
    {
        D.Trace("[PlayerController] run");

        while (_active)
        {
            if (!_paused)
            {
                transform.Translate(Vector2.up * _currentSpeed * Time.deltaTime);
                transform.Rotate(0, 0, Input.GetAxis("Horizontal") * -rotateSpeed * Time.deltaTime);
                GameManager.Instance.BackgroundCameraController.ChangeRotation(transform.rotation);
                _currentSpeed = Mathf.Lerp(_currentSpeed, playerSpeed, Time.deltaTime);
            }
            yield return null;
        }
    }

    //  thanks to http://forum.unity3d.com/threads/tail-follow-script.81635/
    private IEnumerator body()
    {
        D.Trace("[PlayerController] body");

        while (_active)
        {
            if (!_paused)
            {
                D.Fine("- breadcrumbs {0}", breadcrumbs.Count);

                float headDisplacement = (transform.position - breadcrumbs.ElementAt(0)).magnitude;

                if (headDisplacement >= bodySpacing)
                {
                    breadcrumbs.RemoveAt(breadcrumbs.Count - 1);
                    breadcrumbs.Insert(0, transform.position);
                    angles.RemoveAt(angles.Count - 1);
                    angles.Insert(0, transform.eulerAngles);
                    headDisplacement = headDisplacement % bodySpacing;
                }

                if (bodySegments.Count > 0)
                {
                    if (headDisplacement != 0)
                    {
                        Vector3 pos = Vector3.Lerp(breadcrumbs.ElementAt(1), breadcrumbs.ElementAt(0), headDisplacement / bodySpacing);
                        pos = new Vector3(pos.x, pos.y, transform.position.z + 0.01f);
                        bodySegments.ElementAt(0).transform.position = pos;
                        bodySegments.ElementAt(0).transform.eulerAngles = angles.ElementAt(0);

                        for (int i = 1; i < bodySegments.Count; i++)
                        {
                            pos = Vector3.Lerp(breadcrumbs.ElementAt(i + 1), breadcrumbs.ElementAt(i), headDisplacement / bodySpacing);
                            pos = new Vector3(pos.x, pos.y, transform.position.z + i * 0.01f);
                            bodySegments.ElementAt(i).transform.position = pos;
                            bodySegments.ElementAt(i).transform.eulerAngles = angles.ElementAt(i);
                        }
                    }
                }

            }
            yield return null;
        }
    }

    //  MONO

    void Start()
    {
        D.Detail("[PlayerController] Start");
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        D.Trace("[PlayerController] OnTriggerEnter2D");

        if (collider.tag == "Mob")
        {
            GameManager.Instance.SoundController.PlaySound("eat");
            MobData mobData = collider.gameObject.GetComponent<MobObject>().MobData;
            _currentSpeed = _currentSpeed * 0.5f;
            AddBodyPart();
            _playerWeight += mobData.MobWeight;
            GameManager.Instance.GameController.Score += mobData.MobPoints;
            GameManager.Instance.GameGui.Refresh();
            Destroy(collider.gameObject);
            CollectedEarthlings += 1;
        }

        if (collider.tag == "Object")
        {
            GameManager.Instance.PrefabController.CreateObject("smoke", collider.transform.position);
            GameManager.Instance.SoundController.PlaySound("hit");
            _currentSpeed = _currentSpeed * 0.25f;
            GameManager.Instance.GameController.Score += 10;
            GameManager.Instance.GameGui.Refresh();
            Destroy(collider.gameObject);
            RemoveBodyPart();
            CollectedRocks += 1;
        }

        if (collider.tag == "Ice")
        {
            GameManager.Instance.SoundController.PlaySound("pickupice");
            _currentSpeed = _currentSpeed * 0.5f;
            GameManager.Instance.GameController.Score += 500;
            GameManager.Instance.EagleController.Health += 5;
            GameManager.Instance.GameGui.Refresh();
            Destroy(collider.gameObject);
            CollectedIce += 1;
        }

        if (collider.tag == "Gem")
        {
            GameManager.Instance.SoundController.PlaySound("pickupgem");
            _currentSpeed = _currentSpeed * 0.5f;
            GameManager.Instance.GameController.Score += 100;
            GameManager.Instance.GameGui.Refresh();
            Destroy(collider.gameObject);
            CollectedGems += 1;
        }

        if (collider.tag == "Bonus")
        {
            GameManager.Instance.SoundController.PlaySound("pickupbonus");
            _currentSpeed = _currentSpeed * 0.5f;
            GameManager.Instance.GameController.Score += 1000;
            GameManager.Instance.GameGui.Refresh();
            Destroy(collider.gameObject);
            CollectedJewels += 1;
        }

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        D.Trace("[PlayerController] OnCollisionEnter2D");
        _currentSpeed = 0.0f;
    }

    void OnEnable()
    {
        D.Trace("[PlayerController] OnEnable");
        GameManager.Instance.PlayerController = this;
    }
}
