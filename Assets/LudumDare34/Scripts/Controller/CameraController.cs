﻿using UnityEngine;
using System.Collections;
using SpriteTile;

public class CameraController : PauseObject
{
    public Transform playerTransform;
    public Transform eagleTransform;
    public float zoomSpeed;
    public float zoomMax;
    public float zoomFactor;

    public bool rotate;

    private CameraFit cameraFit;
    private Coroutine c1;

    public override void Activate()
    {
        base.Activate();
        c1 = StartCoroutine(run());
    }

    public override void Deactivate()
    {
        base.Deactivate();

        if (c1 != null)
            StopCoroutine(c1);
    }

    private IEnumerator run()
    {
        D.Trace("[CameraController] run");
        //transform.Translate(new Vector2(Input.GetAxis("Horizontal") * levelSpeed * Time.deltaTime, Input.GetAxis("Vertical") * levelSpeed * Time.deltaTime));

        while (_active)
        {
            if (!_paused)
            {
                transform.position = playerTransform.position;
                if (rotate)
                {
                    Tile.CameraRotationZ(playerTransform.eulerAngles.z);
                }

                float dist = Vector3.Distance(playerTransform.transform.position, eagleTransform.transform.position);
                D.Fine("- distance is {0}", dist);

                float d = 15 + dist;

                if (d < 15)
                {
                    d = 15;
                }

                if (d > zoomMax)
                {
                    d = zoomMax;
                }

                d = Mathf.Lerp(cameraFit.UnitsSize, d, Time.deltaTime * zoomSpeed);
                cameraFit.UnitsSize = d;
                cameraFit.ComputeResolution();

            }
            yield return null;
        }
	}

    void OnEnable()
    {
        D.Trace("[CameraController] OnEnable");
        GameManager.Instance.CameraController = this;
        cameraFit = GetComponent<CameraFit>();
    }
}
