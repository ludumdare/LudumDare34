﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicController : MonoBehaviour 
{
    public AudioClip[] Clips;

    private AudioSource _audioSource;

    private Dictionary<string, AudioClip> _clips;

    public void PlayMusic(string name)
    {
        playMusic(_clips[name]);
    }

    //	PRIVATE

    private void playMusic(AudioClip clip)
    {

        playMusic(clip, 1.0f, 0.5f);
    }

    private void playMusic(AudioClip clip, float pitch, float volume)
    {

        _audioSource.clip = clip;
        _audioSource.volume = volume;
        _audioSource.pitch = pitch;
        _audioSource.loop = true;
        _audioSource.Play();
    }

    void Start()
    {
        _clips = new Dictionary<string, AudioClip>();
        foreach (AudioClip clip in Clips)
        {
            _clips.Add(clip.name, clip);
        }
    }

    void OnEnable()
    {
        GameManager.Instance.MusicController = this;
        _audioSource = gameObject.AddComponent<AudioSource>();
    }

    void OnDisable()
    {
    }
}
