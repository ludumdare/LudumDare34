﻿using UnityEngine;
using System.Collections;
using SpriteTile;
using System.Collections.Generic;

public enum GameStates
{
    None,
    Home,
    Options,
    About,
    Game,
    HiScore,
    Stats,
    Win,
    Over,
    Thanks
}

public class GameController : MonoBehaviour
{
    
    private struct LayerTile
    {
        public int TileNumber;
        public Int2 TilePosition;
        public int TileLayer;
    }

    public LevelData LevelData;
    public Material TileMaterial;
    public GameObject Mob;
    public GameObject Light;
    public GameObject Rock;
    public GameObject Birdies;
    public GameObject IceCrystal;
    public GameObject Bonus;
    public GameObject Gem;

    public int Score { get; set; }
    public int Lives { get; set; }
    public int Level { get; set; }
    public int TotalGems { get; set; }
    public int TotalJewels { get; set; }
    public int TotalIce { get; set; }
    public int TotalEarthlings { get; set; }
    
    private static int TILE_LAYER_DEFAULT = 0;
    private static int TILE_LAYER_BACKGROUND = 1;
    private static int TILE_LAYER_CAVERN = 2;
    private static int TILE_LAYER_WALL = 3;
    private static int TILE_LAYER_DECORATOR = 4;
    private static int TILE_LAYER_OBJECT = 5;
    private static int TILE_LAYER_MOB = 6;
    private static int TILE_LAYER_PLAYER = 7;
    private static int TILE_LAYER_FOREGROUND = 8;

    private List<Vector2> waypoints = new List<Vector2>();

    private GameStates gameState;
    private LevelItem currentLevel;
    private GameObject mobParent;
    private GameObject playerParent;
    private GameObject objectParent;


    //  PUBLIC

    public void StartGame()
    {
        gameState = GameStates.Game;
        showGui();
        Level = 0;
        NextLevel();
    }

    public void StartAbout()
    {
        gameState = GameStates.About;
        showGui();
    }

    public void StartOptions()
    {
        gameState = GameStates.Options;
        showGui();
    }

    public void StartStats()
    {
        gameState = GameStates.Stats;
        showGui();
    }

    public void StartWin()
    {
        stopLevel();
        gameState = GameStates.Win;
        showGui();
        GameManager.Instance.SoundController.PlaySound("win");
        GameManager.Instance.WinGui.TallyScore();
    }

    public void StartOver()
    {
        stopLevel();
        gameState = GameStates.Over;
        showGui();
        GameManager.Instance.SoundController.PlaySound("lose");
    }

    public void StartHome()
    {
        stopLevel();
        Tile.EraseLevel();
        GameManager.Instance.EagleController.StartingPosition(new Int2(-10000, -10000));
        GameManager.Instance.PlayerController.StartingPosition(new Int2(-10000, -10000));
        Birdies.transform.position = Vector3.one * -10000;
        gameState = GameStates.Home;
        showGui();
        GameManager.Instance.SoundController.PlaySound("main", true);
    }

    public void StartThanks()
    {
        gameState = GameStates.Thanks;
        showGui();
    }

    public void NextLevel()
    {
        deactivateObjects();

        Level += 1;

        if (Level > LevelData.data.Count)
        {
            StartThanks();
            return;
        }

        currentLevel = LevelData.data[Level - 1].levelItem;
        startLevel();
    }

    public void ReplayLevel()
    {
        currentLevel = LevelData.data[Level - 1].levelItem;
        startLevel();
    }

    //  PRIVATE

    private void stopLevel()
    {
        GameManager.Instance.SoundController.StopSound();

        deactivateObjects();

        if (mobParent != null)
        {
            Destroy(mobParent.gameObject);
        }

        if (objectParent != null)
        {
            Destroy(objectParent.gameObject);
        }

    }

    private void activateObjects()
    {
        GameManager.Instance.CameraController.Activate();
        GameManager.Instance.PlayerController.Activate();
        GameManager.Instance.EagleController.Activate();
    }

    private void deactivateObjects()
    {
        GameManager.Instance.CameraController.Deactivate();
        GameManager.Instance.PlayerController.Deactivate();
        GameManager.Instance.EagleController.Deactivate();
    }

    private void startLevel()
    {
        D.Trace("[GameController] StartLevel");

        GameManager.Instance.SoundController.StopSound();
        GameManager.Instance.SoundController.PlaySound("game", true);

        gameState = GameStates.Game;
        showGui();

        waypoints.Clear();

        TotalEarthlings = 0;
        TotalGems = 0;
        TotalIce = 0;
        TotalJewels = 0;

        Tile.SetCamera(Camera.main);
        Tile.SetTileMaterial(TileMaterial);
        Tile.LoadLevel(currentLevel.LevelText);

        createPlayers();
        createMobs();
        createObjects();

        GameManager.Instance.SoundController.PlaySound("rumble", true);
        GameManager.Instance.SoundController.PlaySound("twinkle", true);

        GameManager.Instance.EagleController.SetWaypoints(waypoints);
        GameManager.Instance.EagleController.Health = 25;

        activateObjects();

    }

    private IEnumerable getLayerTiles(int layer)
    {
        int _mapW = Tile.GetMapSize().x;
        int _mapH = Tile.GetMapSize().y;

        for (int x = 0; x < _mapW; x++)
        {
            for (int y = 0; y < _mapH; y++)
            {
                TileInfo tile = Tile.GetTile(new Int2(x, y), layer);
                if (tile.tile >= 0)
                {
                    D.Fine("- using tile {0}", tile.tile);
                    Int2 pos = new Int2(x, y);
                    LayerTile layerTile = new LayerTile();
                    layerTile.TileLayer = layer;
                    layerTile.TileNumber = tile.tile;
                    layerTile.TilePosition = pos;
                    Tile.DeleteTile(pos, layer);
                    yield return layerTile;
                }
            }
        }
    }

    //  create mob stuff
    private void createMobs()
    {
        mobParent = new GameObject("_mobs");
        foreach (LayerTile layerTile in getLayerTiles(TILE_LAYER_MOB))
        {
            createMob(layerTile);
        }
    }

    //  create player stuff
    private void createPlayers()
    {
        foreach(LayerTile layerTile in getLayerTiles(TILE_LAYER_PLAYER))
        {
            createPlayer(layerTile);
        }
    }

    //  create objects
    private void createObjects()
    {
        objectParent = new GameObject("_objects");

        foreach (LayerTile layerTile in getLayerTiles(TILE_LAYER_OBJECT))
        {
            createObject(layerTile);
        }
    }

    private void createPlayer(LayerTile layerTile)
    {
        switch (layerTile.TileNumber)
        {
            case 0:
                Birdies.transform.position = layerTile.TilePosition.ToVector2();
                GameObject.Find("End").transform.position = layerTile.TilePosition.ToVector2();
                break;

            case 7:
                waypoints.Add(layerTile.TilePosition.ToVector2());
                break;

            case 43:
                GameManager.Instance.PlayerController.StartingPosition(layerTile.TilePosition);
                GameManager.Instance.EagleController.StartingPosition(layerTile.TilePosition);
                break;

            case 11:
                GameObject.Find("Start").transform.position = layerTile.TilePosition.ToVector2();
                break;

            default:
                D.Warn("- tile {0} unknown", layerTile.TileNumber);
                break;
        }
    }

    private void createMob(LayerTile layerTile)
    {

        switch (layerTile.TileNumber)
        {

            case 7:
                waypoints.Add(layerTile.TilePosition.ToVector2());
                break;

            case 14:
                GameObject go = (GameObject)Instantiate(Mob, new Vector3(layerTile.TilePosition.x, layerTile.TilePosition.y, -layerTile.TileLayer), Quaternion.identity);
                go.transform.parent = mobParent.transform;
                TotalEarthlings += 1;
                break;

            default:
                D.Warn("- tile {0} unknown", layerTile.TileNumber);
                break;
        }
    }

    private void createObject(LayerTile layerTile)
    {
        switch (layerTile.TileNumber)
        {

            case 18:
                GameObject rock = (GameObject)Instantiate(Rock, new Vector3(layerTile.TilePosition.x, layerTile.TilePosition.y, -layerTile.TileLayer), Quaternion.identity);
                rock.transform.parent = objectParent.transform;
                break;

            case 67:
                GameObject light = (GameObject)Instantiate(Light, new Vector3(layerTile.TilePosition.x, layerTile.TilePosition.y, -layerTile.TileLayer), Quaternion.identity);
                light.transform.parent = objectParent.transform;
                break;

            case 80:
                GameObject bonus = (GameObject)Instantiate(Bonus, new Vector3(layerTile.TilePosition.x, layerTile.TilePosition.y, -layerTile.TileLayer), Quaternion.identity);
                bonus.transform.parent = objectParent.transform;
                TotalJewels += 1;
                break;

            case 97:
                GameObject ice = (GameObject)Instantiate(IceCrystal, new Vector3(layerTile.TilePosition.x, layerTile.TilePosition.y, -layerTile.TileLayer), Quaternion.identity);
                ice.transform.parent = objectParent.transform;
                TotalIce += 1;
                break;

            case 103:
                GameObject gem = (GameObject)Instantiate(Gem, new Vector3(layerTile.TilePosition.x, layerTile.TilePosition.y, -layerTile.TileLayer), Quaternion.identity);
                gem.transform.parent = objectParent.transform;
                TotalGems += 1;
                break;

            default:
                D.Warn("- tile {0} unknown", layerTile.TileNumber);
                break;
        }
    }

    //  show correct gui
    private void showGui()
    {
        GameManager.Instance.GameGui.Hide();
        GameManager.Instance.HomeGui.Hide();
        GameManager.Instance.AboutGui.Hide();
        GameManager.Instance.OptionsGui.Hide();
        GameManager.Instance.StatsGui.Hide();
        GameManager.Instance.WinGui.Hide();
        GameManager.Instance.OverGui.Hide();
        GameManager.Instance.ThanksGui.Hide();

        switch (gameState)
        {
            case GameStates.About:
                GameManager.Instance.AboutGui.Show();
                break;

            case GameStates.Game:
                GameManager.Instance.GameGui.Show();
                break;

            case GameStates.Home:
                GameManager.Instance.HomeGui.Show();
                break;

            case GameStates.Stats:
                GameManager.Instance.StatsGui.Show();
                break;

            case GameStates.Options:
                GameManager.Instance.OptionsGui.Show();
                break;

            case GameStates.Win:
                GameManager.Instance.WinGui.Show();
                break;

            case GameStates.Over:
                GameManager.Instance.OverGui.Show();
                break;

            case GameStates.Thanks:
                GameManager.Instance.ThanksGui.Show();
                break;

        }
    }

    //  MONO

    void Start()
    {
        D.Trace("[GameController] Start");
        StartHome();
    }

    void OnEnable()
    {
        D.Trace("[GameController] OnEnable");
        GameManager.Instance.GameController = this;
        gameState = GameStates.Home;
    }
}
