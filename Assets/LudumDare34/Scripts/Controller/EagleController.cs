﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System.Linq;
using SpriteTile;

public class EagleController : PauseObject
{
    public float EagleSpeed;
    public bool ShowWaypoints;

    public int Health { get; set; }

    private Coroutine c1;
    private Tween tween;
    private List<Vector2> waypoints;

    //  PUBLIC

    public override void Activate()
    {
        base.Activate();
        c1 = StartCoroutine(run());
    }

    public override void Deactivate()
    {
        base.Deactivate();

        if (c1 != null)
        {
            tween.Kill();
            StopCoroutine(c1);
        }
    }

    public void SetWaypoints(List<Vector2> waypoints)
    {
        D.Log("- there are {0} waypoints", waypoints.Count);

        this.waypoints = waypoints.OrderBy(o => o.y).ToList();
    }

    public void StartingPosition(Int2 pos)
    {
        transform.position = pos.ToVector2() + (Vector2.down * 5);
    }

    //  PRIVATE

    private IEnumerator run()
    {
        while(_active)
        {
            while(!_paused)
            {
                foreach(Vector2 waypoint in waypoints)
                {
                    GameManager.Instance.SoundController.PlaySound("waypoint");
                    tween = transform.DOMove(waypoint, EagleSpeed).SetEase(Ease.Linear);
                    yield return tween.WaitForCompletion();
                }
            }
            yield return null;
        }
    }

    //  MONO


    void OnTriggerEnter2D(Collider2D collider)
    {
        D.Trace("[EagleController] OnTriggerEnter2D");

        if (collider.gameObject.tag == "End")
        {
            GameManager.Instance.GameController.StartWin();
            return;
        }

        GameManager.Instance.PrefabController.CreateObject("hit", collider.transform.position);
        GameManager.Instance.PrefabController.CreateObject("smoke", collider.transform.position);
        GameManager.Instance.SoundController.PlaySound("hit");
        Health -= 1;
        Destroy(collider.gameObject);
        GameManager.Instance.GameGui.Refresh();

        if (Health <= 0)
        {
            GameManager.Instance.SoundController.PlaySound("death");
            GameManager.Instance.GameController.StartOver();
        }
    }

    void Start ()
    {
	
	}
	
	void OnEnable ()
    {
        GameManager.Instance.EagleController = this;
	}

    void OnDrawGizmos()
    {
        if (waypoints == null)
            return;

        if (ShowWaypoints)
        {
            if (waypoints.Count > 0)
            {
                Vector2 last = Vector2.zero;
                foreach(Vector2 w in waypoints)
                {
                    Gizmos.color = Color.yellow;
                    Gizmos.DrawCube(w, Vector2.one);
                    
                    if (last != Vector2.zero)
                    {
                        Gizmos.color = Color.cyan;
                        Gizmos.DrawLine(last, w);
                    }
                    last = w;
                }
            }
        }
    }
}
