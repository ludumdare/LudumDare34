﻿using UnityEngine;
using System.Collections;
using SpriteTile;

public class BackgroundCameraController : MonoBehaviour
{
    public TextAsset level;
    public bool rotate;

    public void ChangeRotation(Quaternion q)
    {
        if (rotate)
        {
            transform.rotation = q;
        }
    }

	void Start ()
    {
        //D.Trace("[BackgroundCameraController] Start");
        //Tile.SetCamera(GetComponent<Camera>());
        //Tile.LoadLevel(level);
    }

    void OnEnable()
    {
        GameManager.Instance.BackgroundCameraController = this;
    }

}
