﻿using UnityEngine;
using System.Collections;

public class LightObject : MonoBehaviour
{
    public Color[] colors;

	// Use this for initialization
	void Start ()
    {
        GetComponent<Light>().color = colors[Random.Range(0, colors.Length)];
	}
	
}
