﻿using UnityEngine;
using System.Collections;

public class ActiveObject : MonoBehaviour
{
    protected bool _active;

    public virtual void Activate()
    {
        D.Trace("[ActiveObject] Activate");
        _active = true;
    }

    public virtual void Deactivate()
    {
        D.Trace("[ActiveObject] Deactivate");
        _active = false;
    }
}
