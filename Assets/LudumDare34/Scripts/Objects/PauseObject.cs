﻿using UnityEngine;
using System.Collections;

public class PauseObject : ActiveObject
{
    protected bool _paused;

    public virtual void Pause()
    {
        D.Trace("[PauseObject] Pause");
        _paused = true;
    }

    public virtual void Resume()
    {
        D.Trace("[PauseObject] Resume");
        _paused = false;
    }
}
