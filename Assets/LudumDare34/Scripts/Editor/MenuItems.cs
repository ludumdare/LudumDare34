﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class MenuItems
{

    [MenuItem("Assets/Create/Mob Data")]
    public static void CreateMobData()
    {
        MobData asset = ScriptableObject.CreateInstance<MobData>();
        AssetDatabase.CreateAsset(asset, "Assets/MobData.asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Level Data")]
    public static void CreateLevelData()
    {
        LevelData asset = ScriptableObject.CreateInstance<LevelData>();
        AssetDatabase.CreateAsset(asset, "Assets/LevelData.asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Level Item Data")]
    public static void CreateLevelItemData()
    {
        LevelItemData asset = ScriptableObject.CreateInstance<LevelItemData>();
        AssetDatabase.CreateAsset(asset, "Assets/LevelItemData.asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}
