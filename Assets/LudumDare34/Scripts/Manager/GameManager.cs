﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoSingleton<GameManager>
{
    public PlayerController PlayerController { get; set; }
    public CameraController CameraController { get; set; }
    public GameController GameController { get; set; }
    public BackgroundCameraController BackgroundCameraController { get; set; }
    public EagleController EagleController { get; set; }

    public GameGuiController GameGui { get; set; }
    public HomeGuiController HomeGui { get; set; }
    public AboutGuiController AboutGui { get; set; }
    public OptionsGuiController OptionsGui { get; set; }
    public StatsGuiController StatsGui { get; set; }
    public WinGuiController WinGui { get; set; }
    public OverGuiController OverGui { get; set; }
    public ThanksGuiController ThanksGui { get; set; }

    public MusicController MusicController { get; set; }
    public SoundController SoundController { get; set; }

    public PrefabController PrefabController { get; set; }

}
